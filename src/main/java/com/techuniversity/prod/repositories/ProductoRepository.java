package com.techuniversity.prod.repositories;

import com.techuniversity.prod.domain.productos.ProductoModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductoRepository extends MongoRepository<ProductoModel, String> {


}
