package com.techuniversity.prod;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SrvProdApplication {

    public static void main(String[] args) {
        SpringApplication.run(SrvProdApplication.class, args);
    }

}
