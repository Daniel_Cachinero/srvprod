package com.techuniversity.prod.controllers;


import com.techuniversity.prod.services.Services;
import jdk.nashorn.api.scripting.JSObject;
import org.bson.Document;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.*;

import javax.print.Doc;
import java.util.List;

@RestController
@RequestMapping("/services")
public class Controller {

    @GetMapping("/service")
    public List getServices() {
        return Services.getAll();
    }

    @PostMapping("/service")
    public String setService(@RequestBody String s) {
        try {
            Services.insert(s);
            return "OK";

        } catch (Exception e) {
            return e.getMessage();

        }
    }

    @PostMapping("/services")
    public String setServices(@RequestBody String s) {
        try {
            Services.insertBatch(s);
            return "OK";
        } catch (Exception e) {
            return e.getMessage();

        }
    }

    @GetMapping("/services")
    public List getService(@RequestBody String s) {
        return Services.getFilter(s);
    }

    @GetMapping("/services/period")
    public List getServicePeriod(@RequestParam String s) {
        Document doc = new Document();
        doc.append("disponibility.period", s);
        return Services.getFilterPeriod(doc);
    }

    @PutMapping("/servicios")
    public String updService(@RequestBody String s) {
        try {
            JSONObject jsonObject = new JSONObject(s);
            String filtro = jsonObject.getJSONObject("filtro").toString();
            String valores = jsonObject.getJSONObject("valores").toString();
            Services.update(filtro, valores);
            return "OK";

        } catch (Exception e) {
            return e.getMessage();

        }
    }
}
