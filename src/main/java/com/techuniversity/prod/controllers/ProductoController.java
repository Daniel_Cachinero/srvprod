package com.techuniversity.prod.controllers;

import com.techuniversity.prod.domain.productos.ProductoModel;
import com.techuniversity.prod.services.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/productos")
public class ProductoController {

    @Autowired
    ProductoService productoService;

    @GetMapping("/productos")
    public List<ProductoModel> getProductos() {
        return productoService.findAll();

    }

    @GetMapping("/productos")
    public List<ProductoModel> getProductosPag(@RequestParam(defaultValue = "-1") String page) {
        int iPage = Integer.parseInt(page);
        if (iPage == -1) {
            return productoService.findAll();
        } else {
            return productoService.findPaginado(iPage);
        }
    }

    @GetMapping("/productos/{id}")
    public Optional<ProductoModel> getProductoId(@PathVariable String id) {
        return productoService.findById(id);
    }

    @PostMapping("/productos")
    public ProductoModel insertProducto(@RequestBody ProductoModel model) {
        productoService.save(model);
        return model;
    }


    @PutMapping("/productos")
    public void updateProductos(@RequestBody ProductoModel model) {
        productoService.save(model);
    }

    @DeleteMapping("/productos")
    public void deleteProducto(@RequestBody ProductoModel model) {
        productoService.deleteProducto(model);
    }
}
