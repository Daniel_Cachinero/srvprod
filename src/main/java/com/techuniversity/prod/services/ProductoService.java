package com.techuniversity.prod.services;

import com.techuniversity.prod.domain.productos.ProductoModel;
import com.techuniversity.prod.repositories.ProductoRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductoService {

    @Autowired
    ProductoRepository productoRepository;


    public List<ProductoModel> findAll() {
        return productoRepository.findAll();
    }

    public Optional<ProductoModel> findById(String id) {
        return productoRepository.findById(id);
    }

    public ProductoModel save(ProductoModel productoModel) {
        return productoRepository.save(productoModel);
    }

    public void deleteProducto(ProductoModel prod) {
        productoRepository.delete(prod);

    }

    public List<ProductoModel> findPaginado(int pag) {
        Pageable pageable = PageRequest.of(pag, 3);
        Page<ProductoModel> page = productoRepository.findAll(pageable);
        return page.getContent();
    }
}
