package com.techuniversity.prod.services;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.*;
import org.bson.Document;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Services {
    MongoCollection<Document> servicios;

    private static MongoCollection<Document> getServiceCollection() {
        ConnectionString connectionString = new ConnectionString("mogodb://localhost:27017");

        MongoClientSettings settings = MongoClientSettings.builder()
                .applyConnectionString(connectionString)
                .build();

        MongoClient client = MongoClients.create(settings);

        MongoDatabase database = client.getDatabase("myDatabase");

        return database.getCollection("services");

    }

    public static List getAll() {
        MongoCollection<Document> collection = getServiceCollection();
        List list = new ArrayList();
        FindIterable<Document> itDoc = collection.find();

        for (Document document : itDoc) {
            list.add(document);

        }
        return list;
    }

    public static void insert(String s) throws Exception {
        Document document = Document.parse(s);

        MongoCollection<Document> collection = getServiceCollection();
        collection.insertOne(document);
    }

    public static void insertBatch(String s) throws Exception {
        Document document = Document.parse(s);
        List<Document> documentList = document.getList("services", Document.class);
        MongoCollection<Document> collection = getServiceCollection();
        collection.insertMany(documentList);
    }

    public static List<Document> getFilter(String s) {
        MongoCollection<Document> collection = getServiceCollection();
        List list = new ArrayList();
        Document document = Document.parse(s);
        FindIterable<Document> itDoc = collection.find(document);
        for (Document value : itDoc) {
            list.add(value);
        }
        return list;
    }

    public static List<Document> getFilterPeriod(Document document) {
        MongoCollection<Document> collection = getServiceCollection();
        List list = new ArrayList();
        FindIterable<Document> itDoc = collection.find(document);
        for (Document value : itDoc) {
            list.add(value);
        }
        return list;
    }

    public static void update(String filtro, String valores) {
        MongoCollection<Document> collection = getServiceCollection();
        Document docFiltro = Document.parse(filtro);
        Document docValores = Document.parse(valores);
        collection.updateOne(docFiltro, docValores);

    }



}
